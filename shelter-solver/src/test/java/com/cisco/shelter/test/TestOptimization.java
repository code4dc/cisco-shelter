package com.cisco.shelter.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import com.cisco.shelter.domain.Building;
import com.cisco.shelter.domain.BuildingInfo;
import com.cisco.shelter.domain.Location;
import com.cisco.shelter.domain.Shelter;
import com.cisco.shelter.domain.WorkRoom;

public class TestOptimization {
	public static void main(String[] args) {
		// Build the Solver
		SolverFactory<Building> solverFactory = SolverFactory
				.createFromXmlResource("com/cisco/shelter/solver/shelterSolverConfig.xml");
		Solver<Building> solver = solverFactory.buildSolver();

		Building unsolvedBuildingProblem = createBuildingProblem();

		// Solve the problem
		Building solvedBuildingProblem = solver.solve(unsolvedBuildingProblem);
		
		printResults(solvedBuildingProblem);
	}

	private static Building createBuildingProblem() {
		Building unsolvedBuildingProblem = new Building();
		Shelter shelter1 = new Shelter(new Location(1, 2, 2), 75);
		Shelter shelter2 = new Shelter(new Location(2, 0, 0), 75);
		Shelter shelter3 = new Shelter(new Location(2, 4, 4), 75);

		unsolvedBuildingProblem.setShelterList(Arrays.asList(shelter1,  shelter2, shelter3));

		WorkRoom workRoom1 = new WorkRoom(new Location(1, 4, 4), 5);
		WorkRoom workRoom2 = new WorkRoom(new Location(1, 3, 4), 5);
		WorkRoom workRoom3 = new WorkRoom(new Location(1, 3, 6), 5);
		WorkRoom workRoom4 = new WorkRoom(new Location(2, 0, 1), 5);

		unsolvedBuildingProblem.setRoomList(Arrays.asList(workRoom1, workRoom2, workRoom3, workRoom4));
		
		BuildingInfo buildingInfo = new BuildingInfo();
		Map<Integer, Location> stairLocations = new HashMap<Integer, Location>();
		stairLocations.put(1, new Location(1, 3, 3));
		stairLocations.put(2, new Location(2, 3, 3));
		
		buildingInfo.setStairLocations(stairLocations);
		unsolvedBuildingProblem.setBuildingInfo(buildingInfo);

		return unsolvedBuildingProblem;
	}
	
	private static void printResults(Building building){
		for(WorkRoom room : building.getRoomList()){
			System.out.println("Room: " + room + " assigned to: " + room.getShelter() + 
					" distance: " + room.getLocation().getDistanceTo(room.getShelter().getLocation(), building.getBuildingInfo().getStairLocations()));
		}
	}

}
