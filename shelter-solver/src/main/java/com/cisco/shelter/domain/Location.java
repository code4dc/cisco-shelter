package com.cisco.shelter.domain;

import java.util.Map;

public class Location {
	private int floor;
	private int row;
	private int column;
	
	public Location(int floor, int row, int column) {
		super();
		this.floor = floor;
		this.row = row;
		this.column = column;
	}
	
	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getColumn() {
		return column;
	}
	public void setColumn(int column) {
		this.column = column;
	}
	
	@Override
	public String toString() {
		return "floor:" + floor + " (" + row + ", " + column + ")";
	}

	public int getDistanceTo(Location destLoc, Map<Integer, Location> stairLocations){
		int distanceInFloors = Math.abs(destLoc.floor - floor);
	
		if(distanceInFloors > 0){
			// Get distance to stairway on starting floor
			Location stairLocationOnStartingFloor = stairLocations.get(floor);
			int roomDistanceToStairs = getDistanceTo(stairLocationOnStartingFloor, stairLocations);
			
			// Get distance to shelter on destination floor
			Location stairLocationOnDestFloor = stairLocations.get(destLoc.floor);
			int destDistanceToStair = stairLocationOnDestFloor.getDistanceTo(destLoc, stairLocations);
			
			return roomDistanceToStairs + destDistanceToStair + distanceInFloors;
			
		}
		return Math.abs(destLoc.row - row) + Math.abs(destLoc.column - column);
	}
	
	
	
	

}
