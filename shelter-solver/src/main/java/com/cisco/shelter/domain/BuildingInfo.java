package com.cisco.shelter.domain;

import java.util.Map;

public class BuildingInfo {
	// Maps the floor number to the location of a stair well on that floor
	private Map<Integer, Location> stairLocations;

	public Map<Integer, Location> getStairLocations() {
		return stairLocations;
	}

	public void setStairLocations(Map<Integer, Location> stairLocations) {
		this.stairLocations = stairLocations;
	}

}
