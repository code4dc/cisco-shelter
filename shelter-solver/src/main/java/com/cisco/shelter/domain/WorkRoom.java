package com.cisco.shelter.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class WorkRoom {
	private Location location;
	private int numOfPeople; 
	
	private Shelter shelter;
	
	public WorkRoom(){
		
	}

	public WorkRoom(Location loc, int numOfPeople) {
		super();
		this.location = loc;
		this.numOfPeople = numOfPeople;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getNumOfPeople() {
		return numOfPeople;
	}

	public void setNumOfPeople(int numOfPeople) {
		this.numOfPeople = numOfPeople;
	}

	@PlanningVariable(valueRangeProviderRefs = "shelterList")
	public Shelter getShelter() {
		return shelter;
	}

	public void setShelter(Shelter shelter) {
		this.shelter = shelter;
	}

	@Override
	public String toString() {
		return "WorkRoom [location=" + location + ", numOfPeople=" + numOfPeople + "]";
	}

}
