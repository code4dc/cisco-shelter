package com.cisco.shelter.domain;

public class Shelter {
	private Location location;
	private int capacity;
	
	public Shelter(){
		
	}
	
	public Shelter(Location loc, int capacity) {
		super();
		this.location = loc;
		this.capacity = capacity;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	@Override
	public String toString() {
		return "Shelter [location=" + location + ", capacity=" + capacity + "]";
	}
	
	

}
