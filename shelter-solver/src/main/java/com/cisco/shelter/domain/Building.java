package com.cisco.shelter.domain;

import java.util.List;
import java.util.Map;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

@PlanningSolution
public class Building {
	private BuildingInfo buildingInfo;
	
	private List<Location> locationList;
	private List<Shelter> shelterList;
	private List<WorkRoom> roomList;
	
	private HardSoftScore score;

	@ProblemFactProperty
	public BuildingInfo getBuildingInfo() {
		return buildingInfo;
	}

	public void setBuildingInfo(BuildingInfo buildingInfo) {
		this.buildingInfo = buildingInfo;
	}

	@PlanningScore
	public HardSoftScore getScore() {
		return score;
	}

	public void setScore(HardSoftScore score) {
		this.score = score;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}

	@ValueRangeProvider(id = "shelterList")
	@ProblemFactCollectionProperty
	public List<Shelter> getShelterList() {
		return shelterList;
	}

	public void setShelterList(List<Shelter> shelterList) {
		this.shelterList = shelterList;
	}

	@PlanningEntityCollectionProperty
	public List<WorkRoom> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<WorkRoom> roomList) {
		this.roomList = roomList;
	}
	
}
